package com.example.brandservice.service;

import com.example.brandservice.VO.Product;
import com.example.brandservice.VO.ResponseTemplateVO;
import com.example.brandservice.entity.Brand;
import com.example.brandservice.repo.BrandRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
@Slf4j
public class BrandService {

    @Autowired
    private final BrandRepository repository;

    @Autowired
    private RestTemplate restTemplate;


    public Brand saveProduct(Brand brand) {
        log.info("qwerty");
        return repository.save(brand);
    }

    public Brand findBrandById(Integer id) {
        log.info("qwerty");
        return repository.findByBrandId(id);
    }

    public ResponseTemplateVO getOne(Integer id) {
        log.info("Inside getUserWithDepartment of UserService");
        ResponseTemplateVO vo = new ResponseTemplateVO();
        Brand brand = repository.findByBrandId(id);

        Product product =
                restTemplate.getForObject("http://PRODUCT-SERVICE/products/" + brand.getProductId()
                        ,Product.class);

        vo.setBrand(brand);
        vo.setProduct(product);

        return  vo;
    }
}
